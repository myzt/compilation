import collections
import glob
import io
import os
import sys


def make_section(topic, level=0):
    if level == 0 :
        return '\n\\section{%s} \n' % topic
    if level == 1 :
        return '\n\\subsection{%s} \n' % topic
    else: 
        raise ValueError('level not supported')


class WordCounter(collections.Counter):
    """Counts words that are not filtered out or on the ingore list"""

    def __init__(*args, **kwds):
        if not args:
            raise TypeError("descriptor '__init__' of 'WordCounter' object needs an argument")
        self = args[0]
        args = args[1:]
        if len(args) > 1:
            raise TypeError('expected at most 1 arguments, got %d' % len(args))

        self.badwords = []

        return super(WordCounter, self).__init__()

    def load_badwordlist(self, filename):
        with open(filename) as f :
            for line in f:
                self.badwords.append(line.strip())

    def latex(self, elem):
        if elem.startswith('\\'):
            return True
        return False

    def badword(self, elem):
        if elem in self.badwords:
            return True
        return False

    def _filter(self, iterable):
        if iterable is not None:
            iterable = [e for e in iterable if not self.badword(e)]
            iterable = [e for e in iterable if not self.latex(e)]
            iterable = [e for e in iterable if len(e) > 1]
            return iterable

    def update(*args, **kwds):
        if not args:
            raise TypeError("descriptor 'update' of 'WordCounter' object "
                            "needs an argument")

        self = args[0]
        args = args[1:]
        if len(args) > 1:
            raise TypeError('expected at most 1 arguments, got %d' % len(args))

        if kwds:
            raise ValueError('kwd arguments not supported')
        iterable = args[0] if args else None
        super(WordCounter, self).update(self._filter(iterable))


class Paper:
    """
    Parses latex document and extracts predefined sections
    abstract, introduction, conclusion
    """
    def __init__(self, filename):
        self.filename = filename
        self.states = {
            'preamble': 0, 
            'document': 1, 
            'abstract': 2, 
            'section': 3, 
            'end': 5
        }
        self.handlers = {
            '\\begin{abstract}': self.begin_abstract,
            '\\end{abstract}': self.end_abstract,
            '\\begin{document}': self.begin_document,
            '\\section{': self.section,
            '\\section*{': self.section,
            '\\end{document}': self.end_document,
            '\\input{': self.input,
            '\\title{': self.title,
            '%': self.comment,
        }
        self.sections_want = [
            'introduction',
            'abstract',
            'conclusion',
            'conclusions',
            'conclusions and future work'
        ]

        self.commands_ignore = ['todo', 'ref']
        self.commands_footnote = ['cite']
        self.characters_escape = ['_']
        self.substitutions = {
            '\\IEEEPARstart{C}{yber}': 'Cyber'
        }

        self.current_section = None
        self.state = self.states['preamble'] 
        self._out = io.StringIO()
        self.wordcount = WordCounter()
        self.wordcount.load_badwordlist('stopwords')

    def get_subj(self, line):
        subj = line.split('{')
        subj = '{'.join(subj[1:])
        subj = subj.split('}')
        subj = '}'.join(subj[:1])
        return subj

    def begin_abstract(self, line):
        self.state = self.states['abstract']
        return make_section('abstract', level=1)

    def end_abstract(self, line):
        self.state = self.states['preamble']

    def begin_document(self, line):
        self.state = self.states['document']

    def end_document(self, line):
        self.state = self.states['end']

    def section(self, line):
        self.current_section = self.get_subj(line).lower()
        self.state = self.states['section']
        if self.current_section in self.sections_want:
            return make_section(self.current_section, level=1)
    
    def title(self, line):
        return make_section(self.get_subj(line))

    def input(self, line):
        dirname = os.path.dirname(self.filename)
        filename = dirname + os.path.sep + self.get_subj(line) + '.tex'
        try:
            self.process_file(filename)
        except FileNotFoundError:
            pass
    
    def comment(self, line):
        # will fail if \?? is used somewhere
        line = line.replace('\\%', '\\??')
        splits = line.split('%')
        if len(splits) > 1:
            res = ''.join(splits[0])
            res = res.replace('\\??', '\\%')
            self.process_line(res)

    def _write(self, output):
        if output: 
            self._out.write(output.strip() + ' ' )

    def process_line(self, line):

        def count(line):
            words = line.lower().split(' ')
            self.wordcount.update(words)

        def sanitize(line):
            if line == '}': return ''

            for tag in self.commands_ignore: 
                line = line.replace('\\%s' % tag, '\\ignore')

            for tag in self.commands_footnote: 
                line = line.replace('\\%s' % tag, '\\footnote')

            for tag in self.characters_escape: 
                line = line.replace('\\%s' % tag, '\\_')

            for k, v in self.substitutions.items():
                line = line.replace(k, v)

            return line

        line = sanitize(line)

        if self.state == self.states['abstract']: 
            count(line)
            return line

        if self.state == self.states['section']:
            count(line)
            if self.current_section in self.sections_want: 
                return line

        return None

    def process_file(self, filename=None):
        if not filename: 
            filename = self.filename
        with open(filename) as f:
            for line in f:
                line = line.strip()
                handled = False
                for handler, f in self.handlers.items():
                    if handler in line:
                        self._write(f(line))
                        handled = True
                if not handled:
                    self._write(self.process_line(line))
        return self

    @property
    def result(self):
        self._out.seek(0)
        return self._out

if __name__ == '__main__':
    papers = {}
    dirname = ''
    if len(sys.argv) > 1:
        dirname = sys.argv[1]
    else: 
        print('usage: %s <dirname>' % os.path.basename(sys.argv[0]))
        sys.exit()

    for name in glob.glob(dirname + os.path.sep + '*/main.tex'):
        papers[name] = Paper(name)
        papers[name].process_file()

    print('\\documentclass{article}')
    print('\\newcommand{\\ignore}[1]{}')
    print('\\begin{document}')
    for k, paper in papers.items():
        print(paper.result.read())
        print(make_section('Most used words', level=1))
        print(paper.wordcount.most_common(10))
        print('\pagebreak')
    print('\\end{document}')
